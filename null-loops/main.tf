/* Alta3 Research - rzfeeser@alta3.com
   Working with "for_each" within a null_resource */


/* a list of local variables */
locals {
  avengers = {
               "Iron Man"= {
                "power"= "money"
                "enemy"= "Iron Monger"}
               "Black Panther"= {
                "power"= "vibranium suit"
                "enemy"= "War Monger"}
               "She-Hulk"= {
                "power"= "super strength"
                "enemy"= "Abomination"}
             }
}

resource "null_resource" "avengers" {
  for_each = tomap(local.avengers)
  triggers = {
    name  = each.key // Iron Man,Black Panther,She-Hulk
    power = each.value // money,vibranium suit,super strength
    enemy = each.value.enemy // Iron Monger,War Monger,Aboninaton
  }
}

/* We want these outputs */
output "avengers" {
  value = null_resource.avengers
}

