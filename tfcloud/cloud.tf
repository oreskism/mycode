terraform {
  cloud {
    organization = "oreski"

    workspaces {
      name = "my-example"
    }
  }
}
