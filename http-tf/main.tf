# The following example shows how to issue an HTTP GET request supplying
# an optional request header.
data "http" "nfl" {
  url = "http://statsapi.web.nhl.com/api/v1/teams"      // API to send HTTP GET to

  # Optional request headers
  request_headers = {
    Accept = "application/json"
  }
}


