# produces an output value named "nfl info"
output "team_stats" {
  description = "API to display team stats"
  value       = data.http.nfl.response_body
}

